
const defaultEngines = require('./engines');
const storeMongoose = require('./stores/mongoose/model');

class Crawler {
  constructor({ store = storeMongoose, options }) {
    this.store = store.init(options.connection);
    this.engines = { ...defaultEngines };
  }

  addEngine(name, engine) {
    this.engines[name] = engine;
  }

  listConfig() {
    return this.store.find({});
  }

  getConfigByUrl(url) {
    const urlParts = new URL(url);
    return this.store.findOne({ sites: urlParts.origin });
  }

  getConfigByName(name) {
    return this.store.findOne({ name });
  }

  setConfig(config) {
    return this.store.updateOne({ name: config.name }, config, { upsert: true });
  }

  unsetConfigByName(name) {
    return this.store.deleteOne({ name });
  }

  async fetch(url, config) {
    const engine = this.engines[config.engine.type];
    const result = await engine.parse(url, config);

    return result;
  }
}

module.exports = Crawler;
