
const mongoose = require('mongoose');

const crawlerSchema = new mongoose.Schema({
  name: { type: String },
  sites: [{ type: String, index: true }],
  engine: {
    type: { type: String },
    options: { type: Object },
  },
  attributes: { type: Object },
});

exports.init = (connection = mongoose, collection = 'Crawler') => (
  connection.model(collection, crawlerSchema)
);
