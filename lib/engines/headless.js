const puppeteer = require('puppeteer');

exports.parse = async (url, config) => {
  const output = {};

  const args = ['--start-maximized'];
  if (process.env.RUN_TEST === 'true') {
    args.push(...['--no-sandbox', '--disable-setuid-sandbox']);
  }

  const launchOptions = { headless: true, args };

  const browser = await puppeteer.launch(launchOptions);
  const page = await browser.newPage();

  try {
    // set viewport and user agent (just in case for nice viewing)
    await page.setViewport({ width: 1366, height: 768 });
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

    // go to the target web
    await page.goto(url);

    // wait for element defined by XPath appear in page
    await page.waitForXPath(config.engine.options.waitForXPath);

    const outputSetters = Object.keys(config.attributes).map((key) => (
      this.setOutput(page, key, config.attributes[key])
    ));

    const evaluatedOutputs = await Promise.all(outputSetters);
    Object.assign(output, Object.fromEntries(evaluatedOutputs));
  } finally {
    await browser.close();
  }

  return output;
};

exports.setOutput = async (page, key, attribute) => {
  const value = await this.getValueFromSelectors(page, attribute);

  if (attribute.output === 'multiple') {
    return [key, value.slice(0, attribute.max)];
  }

  return [key, value[0]];
};

exports.getValueFromSelectors = async (page, attribute) => {
  const evaluations = attribute.selectors.map((v) => (
    this.evaluate(page, attribute.target, v.selector, v.type)
  ));

  const [promises] = await Promise.all(evaluations);

  return promises;
};

/**
 * due to this issue https://github.com/GoogleChrome/puppeteer/issues/1054,
 * the istanbul (coverage reporter) will cause error inside page.evaluate throwing exception.
 * the following "ignore" comment allows istanbul to succeed
 *
 */

/* istanbul ignore next */
exports.evaluate = async (page, targetType, selector, selectorType) => {
  const elHandle = await page.$x(selector);

  await page.evaluate(() => {
    window.getData = (type, el) => { // eslint-disable-line
      switch (type) {
        case 'attr':
          return el.nodeValue;
        case 'text':
          return el.innerText;
        case 'html':
          return el.outerHTML;
        default:
          return el.nodeValue;
      }
    };

    window.cast = (type, value) => { // eslint-disable-line
      switch (type) {
        case 'number':
          return Number(value || 0) || 0;
        case 'boolean':
          return (value || '').toLowerCase === 'false' || true;
        case 'string':
          return (value || '').trim().toString();
        default:
          return value;
      }
    };
  });

  return page.evaluate((
    (tType, sType, ...els) => els.map((el) => (
      window.cast(tType, window.getData(sType, el)) // eslint-disable-line
    ))
  ), targetType, selectorType, ...elHandle);
};
