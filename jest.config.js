module.exports = {
  verbose: true,
  collectCoverage: true,
  coverageReporters: ['text-summary'],
  collectCoverageFrom: [
    'lib/**/*.{js,jsx}',
    '!**/node_modules/**',
    '!**/vendor/**',
  ],
  testEnvironment: 'node',
};
